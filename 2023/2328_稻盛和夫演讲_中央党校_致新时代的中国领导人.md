# 写在前面
- 2023.09.22


在一年前，我似乎已经感受到自己的思想在某种程度上定型了，青少年时代那种世界观收到冲击，人生观肆意生长的感觉完全不存在了，人生似乎就要日复一日的重复下去。

但是最近一年以来，我的心和大脑似乎想是得到了新的养分一样，长出新的枝芽。我又感知到了青少年时期的那种三观的重塑。
不同的是，我已经即将步入而立之年，工作也有四年了，对于社会和人生的了解远胜青少年时期。

这些滋养我思想的，不只是中国的传统文化，也包含了稻盛和夫这样的优秀企业家的著作。

稻盛和夫的哲学思想与道德经以及王阳明的思想有高度的重合，同时又融合了现代社会的生存法则，使得古老的思想能够更好的适用于现代社会。
同时稻盛和夫本人是一位极其优秀的企业家，哲学家，思想家，慈善家。 所以他的作品既可以当做成功学的范本，也可以作为自己为人处事的法则。
所谓的京瓷哲学，就是一部优秀的人生宝典。我甚至觉得阅读稻盛和夫的著作能够更加快速的帮助我们学习道德经，王阳明心学。

因为中日之间的紧张关系，很多时候也不得不考虑是否因为稻盛和夫是一位日本人而选择回避其思想。
后来看到央视对于稻盛和夫的采访，在浙江企业的专场。
举办方说：在中日关系这么紧张的时候（好像是在11年左右），我们依然邀请稻盛先生来中国传授经营之道，就是因为相信大家对于美好社会的向往是超越了国籍的。

我后来也看到稻盛和夫在中国帮助了无数的企业管理者，以及偏远山区的贫苦百姓。对稻盛和夫的思想中“无我利他”还是认同的。

对于日本当前依然存在的军国主义思想等还是要坚决抵制的，但同时我们也可以吸收日本文化中的优秀思想。
日本不仅仅是与中国发生多次冲突的国家，也是与我们文化一脉相承的国家，同处东亚的儒家文化圈，并且有着辉煌与落寞这种极端发展经历。我们能从日本发展历史上学到的是很多的。
我想这可能也是中国的企业家，政府为什么对于稻盛和夫如此重视的原因吧。


- 以下是外交部对稻盛和夫辞世，中方的表态：

在今天（31日）下午的外交部例行记者会上，有记者提问：据报道，日本知名企业家、京瓷公司创始人稻盛和夫8月24日在京都去世。稻盛先生生前曾多次到访中国，在中国同样拥有较高的知名度。中方对此有何评论？

赵立坚表示，中方对稻盛和夫先生去世表示哀悼，向他的家属表示慰问。稻盛和夫先生是全球知名企业家，为推动日本经济、科学、文化事业发展和促进中日两国友好交流合作作出了积极贡献。

赵立坚还说，中日两国各领域联系紧密，利益深度融合，我们希望两国各界共同努力，不断深化交流合作，推动中日关系健康稳定发展。（总台央视记者 吴汶倩 杨毅）



- 在网上很难查到这篇演讲稿，但是由于这片演讲稿过于重要，因此我记录在个人的网站，方便自己阅读，侵权即删。


以下为演讲原文，组织者是时任国家副主席的曾庆红，地点是中央党校。

# 演讲原文
## 致新时代的中国领导人

大家好,我就是刚才承蒙介绍的稻盛和夫。

今天,我对能够有机会来到有着历史传统的中央党校,向肩负着中国未来重任的各位作讲演而深感荣幸。

首先,我向贵校校长曾庆红国家副主席以及为我提供今天这一荣幸机会的有关人士表示最真诚的谢意。

方才,石先生曾大致介绍了一下我的情况。在这里,请允许我再做一些补充。

我于1932年出生于日本西南部的鹿儿岛市,大学时期所学的是化学专业。毕业后我作为一名技术员就职于素有“日本古都”之称的京都市的一家绝缘子生产工厂。
然而,由于自己有一些想法,1959年,当我27岁时,就在京都创办了生产精密陶瓷零部件的京瓷株式会社。创业之初,可谓是赤手空拳。可是,我所开发的精密陶瓷材料逐渐被相关产业广泛应用,随后,我又努力将这一技术推广到了各个领域。
在经过了45年后的今天,京瓷公司已经能够生产精密陶瓷零部件、电子零件、移动电话、打印机、数码复印机以及数码照相机等各种产品,并发展成为了日本具有代表性的企业。

在中国,京瓷集团已先后在上海市、广东省东莞市石龙镇、贵阳市以及天津市设立了庞大的生产基地,从事着电子零部件、数码照相机、打印机、数码复印机以及PHS和CDMA移动电话的生产事业。如今,京瓷公司每年的在华生产额已达到约1000亿日元(约折合80亿元人民币),员工总数也已超过12300人,占京瓷集团海外员工总数的半数以上。去年秋季,经中国政府批准,京瓷设立了负责经销本公司各事业部门产品的京瓷(天津)商贸有限公司。我希望能够通过向中国市场提供我们的各种产品,为中国的经济发展作出贡献。另外,20年前,我在日本创立了经营长途电话及移动通信业务的KDDI公司。以京瓷株式会社为第一大股东的KDDI公司目前已经成为新通信事业的大规模运营商,如今,它已经发展成为仅次于日本的准国有企业垄断性经营企业NTT的日本国内第二大通信运营商(相当于中国的联通)。此外,京瓷集团还拥有宾馆饭店、经营咨询及娱乐等众多的事业群体。我想,像我们这样能够集材料、产品、设备提供到机器制造、通信服务于一体的企业,可以说在全球也并不多见。
据统计,我参与经营的京瓷—KDDI这一企业集团2004年3月份的销售额已经达到了4万亿日元(约折合3200亿人民币)。

在日本,有很多重点中小企业的经营者都表示希望向我学习经营经验。于是,我于1983年在日本开办了无偿讲授经营理论的讲习班,名为“盛和塾”。如今,以日本国内为中心,这样的讲习班已发展达57处,学员数约为3200人。不仅如此,讲习班还扩展到了世界各地,其中,巴西有3处,美国有1处。

同时,出于同样的理由,在中国的江苏、天津、山东、浙江以及四川等地也已经出现了具有“盛和塾”性质的“日本经营哲学研究会”。一些年轻的中国经营者们在那里认真地学习着我的经营理论。

下面进入正题。我今天将以一名企业家的身份,根据自己积累的经验,为大家作题为《致新时代的中国领导人》的讲演。

中国2003年的经济增长率达到了9.1%,对此,日本的媒体此前也有过相关的报道。2001年12月,中国以正式加入WTO为契机,巩固了自己作为世界经济中重要成员的地位。今后,我相信中国经济将会取得更大的发展。
据悉,围绕着入世问题,许多企业家最初曾对中国的未来表示担心。我想,这种不安心理的产生,是由于人们认为中国将会被迫在所有领域遵守全球化规则,并由此产生各种摩擦。
诚然,随着与海外具有实力企业之间的竞争加剧,中国必须处理过剩人员以及庞大的负债问题,并淘汰一些生产率较低的国营企业,使不少企业经历了由这些变化而带来的阵痛。
但在经历诸多阵痛的同时,中国政府满怀信心地进行了经济与产业的结构改革,并最终使许多国内企业有了极大发展。

贵国温家宝总理在不久前的提交全国人大的政府工作报告中指出,“今年的增长目标为7%左右”。我认为,中国经济不仅将持续发展下去,而且会加速向前发展。
不过,在另一方面,伴随着经济的发展,无疑将会出现一些新的威胁到将来发展的问题。诸如人民币汇率、贸易摩擦的激化、劳动力成本的增加、物价上升、地区间贫富差距的拉大、失业率的上升以及环境污染加剧等问题,对于持续发展中的中国来说,需要解决的问题无疑将会有很多。
在这些问题中,最为吃紧的就是不断发生的企业和官僚的渎职和腐败问题。对此,中国政府也在全力进行防止和抑制。然而,我认为产生这些问题的根本原因是在人们的心里。
众所周知,二战后的日本也曾遇到过同样的问题。战后,诞生于自由竞争中的众多日本企业通过相互切磋琢磨,推动了日本经济的不断发展。同时,日本国民由于非常渴望过上富裕的生活而忘我工作,使得日本在战败后不到20年的时间内,发展成为了世界上为数不多的工业大国。

但是,由于多数日本企业在实现了物质方面的富裕之后,忽视了国民生活及地球环境,采取了一味追求企业自身利益的经营态度,从而导致了严重的社会失调。
其中之一就是公害问题。上个世纪60年代,日本进入了高速增长时期后,由扩大工业生产带来的公害问题也日渐显现。由于人们在工业生产中不重视自然环境,只一味地追求经济效益,使得原本山清水秀的日本河流和海洋的水质遭受到严重污染,甚至连鱼类也难以生存下去,工厂周围的天空也整日漂浮着厚重的黑烟。
这一公害问题,最终演变成为有损国民健康并威胁到人类生存的重大社会问题。如今,日本正通过举国上下的共同努力,力图使之得到改善。

然而,许多国民期望“富裕程度”的欲望是无止境的,他们只懂得追求自己的经济利益,想不事生产又能使自己的资产迅速膨胀起来。这就是当时经营者乃至不少国民心中的病态心理。
最终,这一现象催生了80年代的“泡沫经济”。在这一浪潮中,不仅是众多的经营者,甚至连一般的民众也疯狂地投资到股市和房地产,他们边望着自己迅速膨胀的资产暗自得意,边表现出“日本人的资产甚至可以买下整个美国”这样飘飘然的情绪来。
终于,这种膨胀起来的傲慢和欲望便化为工商业者、政治家、官僚等的渎职与丑闻,并迅速迸发出来。

不久,这种泡沫经济最终宣告破灭,日本经济也陷入了由泡沫经济景气反作用所带来的通货紧缩局面。而且,这种战后从未出现过的不景气,在历时十多年后的今天仍在继续。

如此一来,在资本主义体制下,国民的思想颓废导致了经济的停滞不前。因此,我特别希望沿着社会主义市场经济轨道奋进的中国,千万不要再重蹈我国的覆辙。

纵观历史,我们就会发现,构筑了现代世界经济基础的资本主义是起源于基督教社会,尤其是伦理教义极其严格的基督教新教社会。也就是说,早期资本主义的倡导者为虔诚的基督教新教徒们。
他们为了贯彻基督教有关邻人之爱的教义,便尽量使自己的日常生活简朴化,崇尚劳动,将把在企业活动中获取的利润用于社会的发展奉为信条。换言之,“为社会和人类作贡献”,成为了这些新教徒及早期资本主义的道德规范。
人们认为,早期的资本主义是一种身为其倡导者的经营者们,通过经济活动来实践社会道义,为人类社会的进步与发展作贡献的体系,因而也可称之为“为社会积善的体制”。可以说,正是在这一崇高的道德观念前提下,资本主义经济才得以迅速发展。

然而,具有讽刺意味的是,构成资本主义发展原动力的道德观,随着经济的发展却日趋淡化,企业经营的目的以及个人的人生目的也最终蜕变为“只考虑自身利益”的利己主义。如此一来,道德规范开始沦丧,先进的资本主义国家的社会也纷纷走向了颓废。
由于日本不具备欧美各国那样的基督教环境,人们在二战后一味追求经济上的富裕,使得人们对道德、伦理及社会正义的重视程度急剧下降。正如方才所谈及的那样,人们虽然拥有了经济上的富裕,但社会却因偏离了资本主义的本意而日渐陷于颓废。

资本主义决不意味着为了赚钱便可为所欲为。只有拥有了严肃的精神规范,
资本主义才能够正常发挥机能,自由地从事经济活动。我认为,这也同样适用于引入了社会主义市场经济的中国。

如今,作为贵国的邻居,我从目前的中国现状中发现,坚持社会主义体制,实施开放政策的中国,经济正在高速增长。众多的中国经营者们正在以资本主
义社会般的自由度从事着经济活动,国家的经济繁荣也受到世人的广泛称颂。

正如美国创造出了能够代表资本主义经济的“美国之梦”神话一样,如今,不胜枚举的“中国之梦”也正在相继形成。受到人人都拥有成功机会这样的事实以及现实中的成功人士的影响,中国举国上下正涌现出大批希望享受成功喜悦的跃跃欲试者。似乎正是这些人们所释放的巨大能量在推动着中国蓬勃向前发展。

我每次访华时,都会为贵国的高速发展所震惊。不过,我真诚地希望中国能够避免曾经在日本及其他发达国家发生过的类似问题,并在今后也能够持续高速发展下去。

那么,究竟如何去做,才能不使人们的利己之心恶性膨胀,导致国民精神走向颓废呢?我们应该如何避免发生曾经经历过的悲剧,并使国家得到健康发展呢?我认为,作为社会一员的领导人,必须拥有普遍意义上的正确道德观和价值观,并在实践中向人民率先垂范。

据我所知,在中央党校内,各种面向高级干部的培训课程十分重视人格的形成和道德教育这两个方面。我认为,为了使中国获得健康的发展,最重要的是要确立在集团中担负领导责任的领导者的道德观念。
如果领导者的道德水准很低,将会给组织带来重大影响。11世纪中国北宋的大学者、著名诗人苏轼的父亲苏洵曾讲过,“国可以以一人而兴,亦可以一人而亡”。由此得知,人类的历史甚至可以视为领导者的历史。
有关领导者的资质,中国明代的思想家吕新吾花了30年时间在其81岁才完成的著述《呻吟语》中讲,“深沉厚重是第一等资质”。这就是说,作为领导者,最重要的资质是拥有能够经常深入地思考事物的厚重性格。
同时,吕新吾还在《呻吟语》中讲,“能言善辩是第三等资质”。也就是在说,“头脑灵活,有才能,聪明伶俐”的人,仅拥有了第三等资质。

然而,我觉得在全球范围内往往只拥有吕新吾所提及的第三等资质的人,即“聪明伶俐者”会被选为领导者。
诚然,这种人作为有为的人才肯定也会发挥其应有的作用,但这未必代表他们就一定拥有卓越领导人所应具备的人格。
我认为,在当今世界上,包括日本在内,许多资本主义国家都出现了社会性的颓废现象。究其原因,这是由于只具有第三等资质的人被推举为各界的领导者。为了创造更美好的人类社会,我认为,最重要的是我们应该选拔像吕新吾所说的那些第一等资质者,即具有出色领导者人格的人担任各界的领导人。

为了提高以领导者为首的全体国民的道德水准,首先必须有尊重正义和公正的社会性“道德规范”。所幸的是,中国自古就有了这样的规范。拥有优秀的品德,至今仍受到中国人民尊敬的许多历代政治家,执政者及孔子、孟子等诸多思想家们,始终在努力向民众阐释身为国民领导者所应该具备的品德和资质。

例如,《易经》中有“积善之家庆有余”的语句。这是在向人们阐述行善的重要性,它告诉人们,积善之家代代会有幸福相伴。
此外,在《书经》中还有“满招损,谦受益”的警句。这就是说,骄傲自满者将遭受损失,谦虚者则会获得利益。

这些绝非陈腐的“格言故事”,而是向人们昭示正确生活方式的“真理”,也是人们平素最应重视的“道理”。我认为,这正是指导集团、国家走向成功,并使成功得以持续下去的“哲学”。
领导者应率先学习这些古训,并努力付诸实践。因为这是提升国家整体道德水准的前提。领导者以身作则,国民纷纷仿效,这些古训就会作为全国人民的道德观最终得以确立。
拥有这种坚定的伦理道德观念的集团,就一定会取得巨大的发展。换言之,如果能够拥有正确的价值观和判断基准,那么就一定能够获得成功。

我于45年前创立的京瓷集团,时至今日仍在继续地发展壮大。针对这一现象,一些海内外的企业家和经营学者经常问我,“为何京瓷能够一直保持成功?”对此,我总是回答说,“是由于它拥有坚定的经营哲学,并将之与员工共享”。
对于京瓷的成功,有人说是凭借某些技术,也有人说是因为碰巧符合了时代的潮流。但我认为这一看法不正确。
由于京瓷拥有正确的经营哲学,员工也能够将之视为自己的理想,所以,包括干部在内的全体员工都能沿着这一经营哲学路线,付出不逊于任何人的努力,而且,在获得成功后也时时不忘谦虚,方能一直保持成果至今。

我在大学学的是理工科,当初在创办了京瓷这家新公司后,便一直在想:“既然自己对经营一无所知,不如从‘为人什么是正确’这一原点出发,即以是
‘正确的做人道理’还是‘错误的做人准则’,是‘善事’还是‘恶行’为基准对事物进行判断。”于是,我就决定仅以此为依据,不断对发生的事物进行判断。
“正”与“不正”,“善”与“恶”属于最基本的道德规范,是我们从孩提时代每天从父母和老师那里学到的东西,所以,我自信自己对这点非常清楚。
如今细想起来,正是由于当时没有任何经营经验,我仅以最基本的道德伦理观念为出发点投身到经营活动中,才能够取得了今天的成功。

如果将目光转向如今的世界经济领域,我们就会发现,有很多大企业由于丧失了这种道德观念,导致营私舞弊现象和丑闻的发生,结果最终被社会无情地淘汰出局。
做虚假财务报表的美国安然公司、世界电信公司及采取冒充、欺骗手段的日本雪印食品公司等,这些曾风光一时的企业,从丧失道德伦理观发展到作弊,在被曝光后,迅即名誉扫地。
从上述例子可以看出,一旦经营者为私心所吞没,并导致其判断上的失误时,将会为整个集团带来灾难。从这一意义上讲,握有企业经营之舵的经营者,必须随时做出正确的判断。

我想,这种情况不仅限于企业,也同样适用于治理一个国家。也就是说,领导者应该彻底摒弃私欲私利,以利他之心思考事物。这至关重要。简而言之,就是要怀有“无私之心”。贵国的胡**总书记曾指出,要“立党为公”和“权为民所用”,也就是强调了作为国家领导人必须摒弃私心。
谈到领导者的“无私”,让我想起了日本的一位先辈,他就是推动日本由封建国家向现代国家转变的“明治维新”时期的功臣西乡隆盛。
西乡隆盛是我最尊敬的一位历史人物。他的座右铭“敬天爱人”始终被京瓷公司奉为社训,并已植根于所有员工心中。

关于国家领导人的品性,西乡隆盛曾有过如下的阐述。
“置生命、名誉、地位及财产于不顾者最不好对付。然而,不具备这种无私境界者亦将无以成就伟大的事业。”
这就是说,若想成就大事,就必须摒弃一己之私,以无私之心投入到事业中。
可以说,西乡隆盛的这种哲学思想超越了时代,如今依然可以为我们所用。

下面,我还要引用一些。
“堂堂正正地治理国家与宇宙间的自然规律相同,不可以怀有半点私心杂念。无论处于何种情况,都必须让自己心怀公平,走正路,广纳贤明之士,并将国家政权交给能够忠实地履行职责者,这样才符合上苍之意。因此,一旦发现有比自己更胜任者,应该显示出立即让贤的胸襟。”
西乡还说道:“只知利己,亦即只考虑于己有利之事,而不顾他人利益如何的思想,乃为人之大忌。治学不精、事业无成、有过不改、居功自傲,皆因爱己过分而起,故凡此种种皆不可为。”
这段话告诉我们,身为领导者,必须摒弃利己之心,具有自我牺牲之勇气,也就是说,它强调了“无私”的重要性。

正如西乡所言,一个人如果陷入利己之心,便不能作出正确的判断。我认为,现今的各界领导人,必须摆脱“利己”思想的束缚,心中有基于天理大义的坐标,领导集团前进。我认为,这正是构成集团乃至国家发展的基础。
前面我讲了很多,不过是作为一名日本企业家的个人见解,如果这些内容能够被诸位所理解,将是我的荣幸。

最后,我还希望诸位能够了解,已经跻身经济大国之列,而且拥有与之相符的强大军事实力的中国,需要有与之相匹配的崇高“道义”,并有责任成为各国的榜样。
孔子曰,“君子坦荡荡,小人长戚戚。”被称为君子者,永远会胸襟开阔,心境平和坦荡,而不会有小人般的猥琐狭窄心胸。
我认为,这里所说的“君子”,不仅指每一个人,而且也包括国家本身。换言之,人们期待着作为真正大国的中国,在发挥其世界领袖作用的过程中,需要有孔子所提及的“君子”的资质,宽广的胸怀和关怀之心,并与邻国谦虚相处。

这也就是中国革命之父孙中山先生所提倡的“弃霸道,行王道”。
孙中山先生1924年在日本的神户市作过如下内容的演讲。
“西洋的物质文明是科学的文明,后来演变成武力文明,并用来压迫亚洲。这就是中国自古以来所说的‘霸道’文化,亚洲有比它优越的‘王道’文化。王道文化的本质是道德、仁义。
“你们日本民族在吸收欧美霸道文化的同时,也拥有亚洲王道文化的本质。今后,在世界文化的未来面前,到底是成为西洋霸道的看家狗,还是成为亚洲王道的卫士,取决于你们日本国民的认真思考和慎重选择。”

遗憾的是,日本没有倾听孙中山先生的忠告,而是在霸道之中一泻千里。终于尝到了第二次世界大战战败的苦果。
我希望不久的将来,成为经济大国并拥有强大军事实力的中国,一定不能陷入自己一直加以否定的霸权主义,以自古以来中国人十分重视的“以德相报”的胸襟,亦即遵循王道,治理国家,从事经济活动。

我想,这与江泽民前国家主席所强调的“实现德治国家”思想是息息相关的。
中央党校,曾经培养出了众多继承中国共产党的理念,具有卓越才能的领导者,他们领导了20世纪后半叶的中国。

在结束此次讲演之前,我衷心祝愿先后有胡锦涛国家主席、曾庆红国家副主席担任校长的中央党校,在21世纪里,能够培养出众多的中国乃至世界级领导人,为使中国成为受到世界各国尊敬的大国作出贡献。

最后,再次感谢大家。